const URL_DATA = `http://localhost:3000/data-json`;
let content_tabs = document.getElementById('content-json');
let tabs_crew = document.getElementById('tabs-crew');

fetch(`${URL_DATA}`)
    .then(response => response.json())  
    .then(json => {
        let crew = json.crew;
        let images_back = document.getElementById('images_crew');

        crew.forEach(function(data, key) {
            let images = `<img src="${data.images.png}" class="image-crew"/>`;

            let html = `
                <div class="content_crew container_four" id="content_${key}">
                    <div class="flex-center margin_100">
                        <h5 class="susbtitle-crew">${data.role}</h5>
                        <h2 class="tetx-title-two">${data.name}</h2>
                        <p class="paragraph w-70">${data.bio}</p>
                        <div class="owl-dots owl-dots-show"></div>
                    </div>
                    <div class="images_crew">
                        ${images}
                    </div>
                </div> `;
          
            content_tabs.innerHTML += html;
        });
    })    //imprimir los datos en la consola
    .catch(err => console.log('Solicitud fallida', err));


   //AGREGO OWL CARROUSEL   
    setTimeout(() => {
        $('.owl-carousel').owlCarousel({
            nav:true,
            margin:0,
            // autoplay:true,
            // autoplayTimeout:5000,
            // autoplayHoverPause:true,
            loop: true,
            dotsClass: "technology-dots",
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    }, 500);