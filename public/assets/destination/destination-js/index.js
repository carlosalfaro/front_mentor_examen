const URL_DATA = `http://localhost:3000/data-json`;
let content_tabs = document.getElementById('content-json');
let tabs_destination = document.getElementById('tabs-destination');

fetch(`${URL_DATA}`)
    .then(response => response.json())  
    .then(json => {
        let destination = json.destinations;
        let images_back = document.getElementById('images_destination');
        destination.forEach(function(data, key) {
            let images = `<img src="${data.images.png}" class="image-destination" id="image_${key}"/>`;
            console.log(destination);
            let html = `
                <div class="content_destination content_${key}" id="content_${key}">
                    <div class="flex-center">
                        <h2 class="tetx-title">${data.name}</h2>
                        <p class="paragraph w-70">${data.description}</p>
                    </div>
                    <hr class="line-white" />
                    <div class="container_two_column">
                        <div>
                            <h6 class="subtitle_destination">
                                AVG. DISTANCE
                            </h6>
                            <span class="distance">
                               ${data.distance}
                            </span>
                        </div>
                        <div>
                            <h6 class="subtitle_destination">
                                AVG. DISTANCE
                            </h6>
                            <span class="distance">
                                ${data.travel}
                            </span>
                        </div>
                    </div>
                </div> `;
          
            content_tabs.innerHTML += html;
            images_back.innerHTML += images;
            tabs_destination.innerHTML += `<button type="button" class="tabs ${key === 0 ? "active" : ''}" >${data.name}</button>`;
        });

        setTimeout(() => {
            let buttons = document.querySelectorAll('.tabs');
            Array.from(buttons).forEach(function(value, key){
                value.addEventListener('click', function(){
                    showContent(key, value);
                });
            })
        }, 500);

    })    //imprimir los datos en la consola
    .catch(err => console.log('Solicitud fallida', err));


    function showContent(key, value){
        let content = document.getElementsByClassName('content_destination');
        let content_show = document.getElementById(`content_${key}`);
        let image = document.getElementsByClassName('image-destination');
        let image_show = document.getElementById(`image_${key}`);
        let buttons = document.querySelectorAll('.tabs');

        Array.from(content).forEach(function(value, key){
            value.style.display = 'none';
        });

        Array.from(image).forEach(function(value, key){
            value.style.display = 'none';
        });

        Array.from(buttons).forEach(function(value, key){
            value.classList.remove('active');
        })

        content_show.style.display ='block';
        image_show.style.display ='block';
        value.classList.add('active');
    }