let open_menu = document.getElementById('open-menu');
let close_menu = document.getElementById('close-menu');
let menu_mobile = document.getElementById('menu-mobile');


open_menu.addEventListener('click', function(){
    menu_mobile.style.visibility = 'visible';
    menu_mobile.style.zIndex = '9';
    close_menu.style.display = 'block';
    open_menu.style.display = 'none';
});

close_menu.addEventListener('click', function(){
    menu_mobile.style.visibility = 'hidden';
    menu_mobile.style.zIndex = '-1';
    close_menu.style.display = 'none';
    open_menu.style.display = 'block';
});