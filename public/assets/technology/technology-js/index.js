const URL_DATA = `http://localhost:3000/data-json`;
let content_tabs = document.getElementById('content-json');
let tabs_technology = document.getElementById('tabs-technology');

fetch(`${URL_DATA}`)
    .then(response => response.json())  
    .then(json => {
        let technology = json.technology;
        let images_back = document.getElementById('images_technology');
        technology.forEach(function(data, key) {
            let images = `<img src="${data.images.portrait}" class="image-technology desktop" id="image_${key}"/>
            <img src="${data.images.landscape}" class="image-technology mobile" id="image_${key}"/> `;
            console.log(technology);
            let html = `
                <div class="content_technology content_${key}" id="content_${key}">
                    <div class="flex-center">
                        <p class="paragraph padding-bottom">THE TERMINOLOGY...</p>
                        <h2 class="tetx-title-two">${data.name}</h2>
                        <p class="paragraph w-70">${data.description}</p>
                    </div>
                </div> `;
          
            content_tabs.innerHTML += html;
            images_back.innerHTML += images;
            tabs_technology.innerHTML += `<button type="button" class="tabs-technology  ${key === 0 ? "active" : ''}" >${key+1}</button>`;
        });

        setTimeout(() => {
            let buttons = document.querySelectorAll('.tabs-technology');
            Array.from(buttons).forEach(function(value, key){
                value.addEventListener('click', function(){
                    showContent(key, value);
                });
            })
        }, 500);

    })    //imprimir los datos en la consola
    .catch(err => console.log('Solicitud fallida', err));


    function showContent(key, value){
        let content = document.getElementsByClassName('content_technology');
        let content_show = document.getElementById(`content_${key}`);
        let image = document.getElementsByClassName('image-technology');
        let image_show = document.getElementById(`image_${key}`);
        let buttons = document.querySelectorAll('.tabs-technology');

        Array.from(content).forEach(function(value, key){
            value.style.display = 'none';
        });

        Array.from(image).forEach(function(value, key){
            value.style.display = 'none';
        });

        Array.from(buttons).forEach(function(value, key){
            value.classList.remove('active');
        })

        content_show.style.display ='block';
        image_show.style.display ='block';
        value.classList.add('active');
    }