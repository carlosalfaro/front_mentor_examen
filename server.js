const express = require("express");
const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.static("public"));

app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!');
    res.status(404).send('Not Found!');
});

app.get('/', (req, res) => {
    res.sendFile (__dirname +  '/public/index.html');
});

app.get('/destination', (req, res) => {
    res.sendFile (__dirname +  '/public/destination.html');
});

app.get('/crew', (req, res) => {
    res.sendFile (__dirname +  '/public/crew.html');
});

app.get('/technology', (req, res) => {
    res.sendFile (__dirname +  '/public/technology.html');
});

app.get('/data-json', (req, res) => {
    res.sendFile (__dirname +  '/data/data.json');
});

app.listen(PORT, () => {
    console.log(`El servidor se está ejecutando en ${PORT}/`);
});